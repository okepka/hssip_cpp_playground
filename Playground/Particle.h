#ifndef PARTICLE_H
#define PARTICLE_H

class Particle {

   public:
      Particle(float _pt, float _eta):
         m_pt(_pt), m_eta(_eta) {}


      inline float & pt(){ return m_pt; }
      inline float & eta(){ return m_eta; }

   private:

      float m_pt;
      float m_eta;
};


#endif
