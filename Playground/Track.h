#ifndef TRACK_H
#define TRACK_H

#include "Particle.h"
#include <string>

class Track : public Particle {

   public: 
      Track(float _pt, float _eta, int _nhits):
         Particle(_pt, _eta), m_nhits(_nhits)
   {
   }

    inline float nhits(){ return m_nhits; }

    std::string print();

   private:
      int   m_nhits;


};

#endif

