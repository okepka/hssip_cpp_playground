#include "Track.h"
#include <string>
#include <sstream>


std::string Track::print() {


   std::ostringstream str;
   str << "Track pt=" << pt() << " eta= " << eta() << " nhits= " << nhits();

   return str.str();

}
