
BUILD=build
INCLUDE=-IPlayground
STDLIB=-lstdc++

PRG=$(patsubst utils/%.cxx, bin/%, $(wildcard utils/*.cxx))
SRC=$(wildcard src/*)
LIBS=$(BUILD)/Track.o


all: $(PRG) $(LIBS)
 
$(BUILD)/Track.o: build/%.o: src/%.cxx Playground/%.h build
	gcc -c -o $@ $< $(INCLUDE) $(STDLIB)

$(PRG): bin/%: utils/%.cxx bin $(LIB)
	gcc -o $@ $< $(LIBS) $(INCLUDE) $(STDLIB)

bin:
	mkdir -p bin

build:
	mkdir -p build




clean:
	rm -rf bin
	rm -rf build

