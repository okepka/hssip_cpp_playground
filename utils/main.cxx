
#include "Track.h"
#include <vector>
#include <iostream>

int main() {


   // ukazka pouzivani statickych prommennych ve vektoru
   std::vector<Track> tracks;
   tracks.push_back(  Track(10, 0   , 2) ); 
   tracks.push_back(  Track(20, 2.5 , 7) ); 
   tracks.push_back(  Track(30, -2.5, 4) ); 


   for(unsigned int i = 0; i< tracks.size(); i++)
   {
      std::cout << tracks[i].print() << std::endl;;

      // identicke
      Track & t = tracks[i];    // copiruj odkaz, ne hodnotu
      t.print();
      t.pt()+= 5;           // zmeni hodnotu pt tracku ve vektoru

      Track t2 = tracks[i];    // local copy (note no &)
      t2.pt() +=10;         // nezmeni obsah tracks

   }



   // ukazka pouzivani  pointeru
   typedef std::vector<Track*> Track_container;
   Track_container track_container;

   track_container.push_back( new Track(100, 0   , 9) );
   track_container.push_back( new Track(200, 2.5 , 8) );
   track_container.push_back( new Track(300, -2.5, 5) );

   for(unsigned int i = 0; i< track_container.size(); i++)
   {
      Track * ptrk = track_container[i];   // ukazuje primo na objekt, ktery je v kontaineru
      std::cout << ptrk-> print() << std::endl;
   }


}
